package com.raphanum.amocrmtask.model.api;

import com.raphanum.amocrmtask.model.dto.AccountRootDTO;
import com.raphanum.amocrmtask.model.dto.LeadsRootDTO;

import retrofit2.http.GET;
import retrofit2.http.Query;
import rx.Observable;

public interface AmoCrmApi {

    @GET("private/api/v2/json/leads/list")
    Observable<LeadsRootDTO> getLeads(@Query("USER_LOGIN") String login,
                                      @Query("USER_PASSWORD") String password);

    @GET("private/api/v2/json/accounts/current")
    Observable<AccountRootDTO> getAccountData(@Query("USER_LOGIN") String login,
                                              @Query("USER_PASSWORD") String password);
}
