package com.raphanum.amocrmtask.model.dto;

import com.google.gson.annotations.SerializedName;

public class LeadDTO {
    private String name;
    @SerializedName("date_create")
    private long creationDate;
    private String price;
    @SerializedName("status_id")
    private String statusId;

    public String getName() {
        return name;
    }

    public long getCreationDate() {
        return creationDate;
    }

    public String getPrice() {
        return price;
    }

    public String getStatusId() {
        return statusId;
    }
}
