package com.raphanum.amocrmtask.model.parcelable;

import android.os.Parcel;
import android.os.Parcelable;

public class Lead implements Parcelable {
    private String name;
    private String creationDate;
    private String price;
    private Status status;

    public Lead(String name, String creationDate, String price, Status status) {
        this.name = name;
        this.creationDate = creationDate;
        this.price = price;
        this.status = status;
    }

    protected Lead(Parcel in) {
        name = in.readString();
        creationDate = in.readString();
        price = in.readString();
        status = in.readParcelable(Status.class.getClassLoader());
    }

    public static final Creator<Lead> CREATOR = new Creator<Lead>() {
        @Override
        public Lead createFromParcel(Parcel in) {
            return new Lead(in);
        }

        @Override
        public Lead[] newArray(int size) {
            return new Lead[size];
        }
    };

    public String getName() {
        return name;
    }

    public String getCreationDate() {
        return creationDate;
    }

    public String getPrice() {
        return price;
    }

    public Status getStatus() {
        return status;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(name);
        parcel.writeString(creationDate);
        parcel.writeString(price);
        parcel.writeParcelable(status, i);
    }
}
