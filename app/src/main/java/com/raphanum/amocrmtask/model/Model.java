package com.raphanum.amocrmtask.model;

import com.raphanum.amocrmtask.model.parcelable.Lead;

import java.util.List;

import rx.Observable;

public interface Model {

    Observable<List<Lead>> getLeadsWithStatuses();
}
