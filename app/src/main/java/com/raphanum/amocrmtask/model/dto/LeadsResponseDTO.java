package com.raphanum.amocrmtask.model.dto;

import java.util.List;

public class LeadsResponseDTO {
    private List<LeadDTO> leads;

    public List<LeadDTO> getLeads() {
        return leads;
    }
}
