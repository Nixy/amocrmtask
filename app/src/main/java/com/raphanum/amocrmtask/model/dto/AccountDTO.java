package com.raphanum.amocrmtask.model.dto;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AccountDTO {
    @SerializedName("leads_statuses")
    private List<LeadStatusDTO> leadsStatuses;

    public List<LeadStatusDTO> getLeadsStatuses() {
        return leadsStatuses;
    }
}
