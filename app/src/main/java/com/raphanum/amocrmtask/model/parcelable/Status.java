package com.raphanum.amocrmtask.model.parcelable;

import android.os.Parcel;
import android.os.Parcelable;

public class Status implements Parcelable {
    private String name;
    private String color;

    public Status(String name, String color) {
        this.name = name;
        this.color = color;
    }

    protected Status(Parcel in) {
        name = in.readString();
        color = in.readString();
    }

    public static final Creator<Status> CREATOR = new Creator<Status>() {
        @Override
        public Status createFromParcel(Parcel in) {
            return new Status(in);
        }

        @Override
        public Status[] newArray(int size) {
            return new Status[size];
        }
    };

    public String getName() {
        return name;
    }

    public String getColor() {
        return color;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(name);
        parcel.writeString(color);
    }
}
