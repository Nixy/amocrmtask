package com.raphanum.amocrmtask.view;

import com.raphanum.amocrmtask.model.parcelable.Lead;

import java.util.List;

public interface LeadListView {

    void showLoading();

    void hideLoading();

    void showLeads(List<Lead> leads);

    void showEmpty();

    void showError();
}
