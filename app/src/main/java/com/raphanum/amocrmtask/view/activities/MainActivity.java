package com.raphanum.amocrmtask.view.activities;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.raphanum.amocrmtask.R;
import com.raphanum.amocrmtask.di.DaggerLeadListViewComponent;
import com.raphanum.amocrmtask.di.LeadListViewComponent;
import com.raphanum.amocrmtask.di.LeadListViewModule;
import com.raphanum.amocrmtask.model.parcelable.Lead;
import com.raphanum.amocrmtask.presenter.LeadListPresenter;
import com.raphanum.amocrmtask.view.LeadListView;
import com.raphanum.amocrmtask.view.adapters.LeadListAdapter;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

public class MainActivity extends AppCompatActivity implements LeadListView {
    private LeadListViewComponent viewComponent;
    @Inject
    protected LeadListPresenter presenter;
    private RecyclerView recyclerView;
    private LeadListAdapter adapter;
    private static final String LEADS_RETAIN_KEY = "leadsRetainKey";
    private List<Lead> leads;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initViewComponent();
        initRecyclerView();
        initSwipeRefreshLayout();

        if (savedInstanceState != null) {
            leads = savedInstanceState.getParcelableArrayList(LEADS_RETAIN_KEY);
            showLeads(leads);
        } else {
            showEmpty();
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putParcelableArrayList(LEADS_RETAIN_KEY, (ArrayList<? extends Parcelable>) leads);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_refresh:
                presenter.onRefresh();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void initSwipeRefreshLayout() {
        SwipeRefreshLayout swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.leads_swipe_refresh_layout);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorAccent);
        swipeRefreshLayout.setOnRefreshListener(() -> presenter.onRefresh());
    }

    private void initRecyclerView() {
        recyclerView = (RecyclerView) findViewById(R.id.leads_recycler_view);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new LeadListAdapter();
        recyclerView.setAdapter(adapter);
        recyclerView.addItemDecoration(new DividerItemDecoration(this, layoutManager.getOrientation()));
    }

    private void initViewComponent() {
        if (viewComponent == null) {
            viewComponent = DaggerLeadListViewComponent.builder()
                    .leadListViewModule(new LeadListViewModule(this))
                    .build();
        }
        viewComponent.inject(this);
    }

    @Override
    public void showLoading() {
        setRefreshing(true);
    }

    @Override
    public void hideLoading() {
        setRefreshing(false);
    }

    @Override
    public void showLeads(List<Lead> leads) {
        this.leads = leads;
        findViewById(R.id.empty_list_layer).setVisibility(View.GONE);
        recyclerView.setVisibility(View.VISIBLE);
        adapter.setLeads(leads);
    }

    @Override
    public void showEmpty() {
        recyclerView.setVisibility(View.GONE);
        findViewById(R.id.empty_list_layer).setVisibility(View.VISIBLE);
    }

    @Override
    public void showError() {
        Toast.makeText(this, R.string.error, Toast.LENGTH_SHORT).show();
    }

    void setRefreshing(boolean refreshing) {
        SwipeRefreshLayout swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.leads_swipe_refresh_layout);
        swipeRefreshLayout.setRefreshing(refreshing);
    }
}
