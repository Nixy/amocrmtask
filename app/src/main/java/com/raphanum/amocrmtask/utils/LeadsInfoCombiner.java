package com.raphanum.amocrmtask.utils;

import com.raphanum.amocrmtask.model.dto.LeadDTO;
import com.raphanum.amocrmtask.model.parcelable.Lead;
import com.raphanum.amocrmtask.model.parcelable.Status;

import java.util.HashMap;
import java.util.List;

import rx.Observable;
import rx.functions.Func2;

public class LeadsInfoCombiner implements Func2<List<LeadDTO>, HashMap<String,Status>, List<Lead>> {
    @Override
    public List<Lead> call(List<LeadDTO> leadDTOs, HashMap<String, Status> stringStringHashMap) {
        return Observable.from(leadDTOs)
                .map(leadDTO -> new Lead(leadDTO.getName(),
                        LongToDateStringConverter.convert(leadDTO.getCreationDate()),
                        leadDTO.getPrice(),
                        stringStringHashMap.get(leadDTO.getStatusId())))
                .toList()
                .toBlocking()
                .first();
    }
}
