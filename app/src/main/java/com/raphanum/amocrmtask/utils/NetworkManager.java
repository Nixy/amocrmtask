package com.raphanum.amocrmtask.utils;

import com.raphanum.amocrmtask.model.api.AmoCrmApi;
import com.raphanum.amocrmtask.model.dto.AccountRootDTO;
import com.raphanum.amocrmtask.model.dto.LeadsRootDTO;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;

public class NetworkManager {
    private AmoCrmApi api;

    public NetworkManager() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .build();
        api = retrofit.create(AmoCrmApi.class);
    }

    public Observable<LeadsRootDTO> getLeads() {
        return api.getLeads(Constants.USER_LOGIN,
                Constants.USER_PASSWORD);
    }

    public Observable<AccountRootDTO> getAccountData() {
        return api.getAccountData(Constants.USER_LOGIN,
                Constants.USER_PASSWORD);
    }
}
