package com.raphanum.amocrmtask.di;

import com.raphanum.amocrmtask.presenter.LeadListPresenter;
import com.raphanum.amocrmtask.view.LeadListView;

import dagger.Module;
import dagger.Provides;

@Module
public class LeadListViewModule {
    private LeadListView view;

    public LeadListViewModule(LeadListView view) {
        this.view = view;
    }

    @Provides
    LeadListPresenter provideLeadListPresenter() {
        return new LeadListPresenter(view);
    }
}
