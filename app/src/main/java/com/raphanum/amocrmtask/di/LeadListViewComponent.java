package com.raphanum.amocrmtask.di;

import com.raphanum.amocrmtask.view.activities.MainActivity;

import dagger.Component;

@Component(modules = {LeadListViewModule.class})
public interface LeadListViewComponent {

    void inject(MainActivity activity);
}
