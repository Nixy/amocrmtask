package com.raphanum.amocrmtask;

import android.app.Application;

import com.raphanum.amocrmtask.di.AppComponent;
import com.raphanum.amocrmtask.di.DaggerAppComponent;

public class App extends Application {
    private static AppComponent component;

    public static AppComponent getComponent() {
        return component;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        component = DaggerAppComponent.builder().build();
    }
}
