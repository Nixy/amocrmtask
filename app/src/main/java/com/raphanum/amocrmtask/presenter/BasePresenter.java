package com.raphanum.amocrmtask.presenter;

import com.raphanum.amocrmtask.App;
import com.raphanum.amocrmtask.model.Model;

import javax.inject.Inject;

public abstract class BasePresenter {
    @Inject
    protected Model model;

    public BasePresenter() {
        App.getComponent().inject(this);
    }
}
