package com.raphanum.amocrmtask.presenter;

import com.raphanum.amocrmtask.view.LeadListView;

import javax.inject.Inject;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class LeadListPresenter extends BasePresenter {
    private LeadListView view;

    @Inject
    public LeadListPresenter() {
    }

    public LeadListPresenter(LeadListView view) {
        this.view = view;
    }

    public void onRefresh() {
        loadLeadsWithStatuses();
    }

    private void loadLeadsWithStatuses() {
        view.showLoading();
        model.getLeadsWithStatuses()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(leads -> {
                            view.hideLoading();
                            if (leads.size() > 0) {
                                view.showLeads(leads);
                            } else {
                                view.showEmpty();
                            }
                        },
                        throwable -> {
                            view.hideLoading();
                            view.showError();
                            throwable.printStackTrace();
                        });
    }
}
